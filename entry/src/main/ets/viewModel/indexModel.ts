export interface imgObj {
  id: number | string
  // @ts-ignore
  url: Resource
}

export class ModuleItem {
  id: string
  // @ts-ignore
  img: ResourceStr
  title: string
  price: number
  des: string
  path: string

  // @ts-ignore
  constructor(id: string, img: ResourceStr, title: string, price: number, des: string, path: string = '') {
    this.id = id
    this.img = img
    this.title = title
    this.price = price
    this.des = des
    this.path = path
  }
}


export const bannerList: imgObj[] = [
  { id: 1, url: $r('app.media.banner1') },
  { id: 2, url: $r('app.media.banner2') },
  { id: 3, url: $r('app.media.banner3') },
  { id: 4, url: $r('app.media.banner4') },
  { id: 5, url: $r('app.media.banner5') },
  { id: 6, url: $r('app.media.banner6') },
  { id: 7, url: $r('app.media.banner7') },
  { id: 8, url: $r('app.media.banner8') },
  { id: 9, url: $r('app.media.banner9') },
  { id: 10, url: $r('app.media.banner10') },
  { id: 11, url: $r('app.media.banner11') }
]

export const utils: Array<ModuleItem> = [
  new ModuleItem('module1', $r('app.media.item7'), 'ImgScale', 99.99, '', 'pages/TransformImg'),
  new ModuleItem('module2', $r('app.media.shop3'), '商品列表', 99.99, '', 'pages/ShopList'),
  new ModuleItem('module3', $r('app.media.item7'), 'Scorll', 99.99, '', 'pages/ScorllPage',),
  new ModuleItem('module4', $r('app.media.item7'), 'Task', 0, '', 'pages/TaskPlan',),
  new ModuleItem('module5', $r('app.media.item6'), 'StorageEg', 99.99, '', 'pages/StorageEg',),
  new ModuleItem('module6', $r('app.media.item6'), 'CustomDialog', 99.99, '', 'pages/CustomDialog',),
  new ModuleItem('module7', $r('app.media.item6'), 'axios/dayjs', 99.99, '', 'pages/AxiosPage',),
  new ModuleItem('module8', $r('app.media.item6'), '这是一个超长的测试标题', 99.99, '', '',),
]


export const shopList: Array<ModuleItem> = [
  new ModuleItem('module2', $r('app.media.shop2'), '百褶裙半身裙', 129.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', ''),
  new ModuleItem('module2', $r('app.media.shop2'), '百褶裙半身裙', 129.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', ''),
  new ModuleItem('module1', $r('app.media.shop1'), '百褶裙半身裙', 109.99, 'Guozii秋冬新款美式复古撞色拼接设计t恤女2023早秋宽松休闲上衣', ''),
  new ModuleItem('module3', $r('app.media.shop3'), '百褶裙半身裙', 99.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module4', $r('app.media.shop4'), '百褶裙半身裙', 79.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module5', $r('app.media.shop5'), '百褶裙半身裙', 139.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module6', $r('app.media.shop6'), '百褶裙半身裙', 145.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module7', $r('app.media.shop7'), '百褶裙半身裙', 289.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module8', $r('app.media.shop8'), '百褶裙半身裙', 389.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module1', $r('app.media.shop9'), '百褶裙半身裙', 109.99, 'Guozii秋冬新款美式复古撞色拼接设计t恤女2023早秋宽松休闲上衣', ''),
  new ModuleItem('module2', $r('app.media.shop10'), '百褶裙半身裙', 129.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', ''),
  new ModuleItem('module3', $r('app.media.shop1'), '百褶裙半身裙', 99.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module4', $r('app.media.shop2'), '百褶裙半身裙', 79.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module5', $r('app.media.shop3'), '百褶裙半身裙', 139.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module6', $r('app.media.shop4'), '百褶裙半身裙', 145.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module7', $r('app.media.shop5'), '百褶裙半身裙', 289.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module8', $r('app.media.shop6'), '百褶裙半身裙', 389.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module7', $r('app.media.shop7'), '百褶裙半身裙', 289.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module8', $r('app.media.shop8'), '百褶裙半身裙', 389.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module1', $r('app.media.shop9'), '百褶裙半身裙', 109.99, 'Guozii秋冬新款美式复古撞色拼接设计t恤女2023早秋宽松休闲上衣', ''),
  new ModuleItem('module2', $r('app.media.shop10'), '百褶裙半身裙', 129.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', ''),
  new ModuleItem('module3', $r('app.media.shop1'), '百褶裙半身裙', 99.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module4', $r('app.media.shop2'), '百褶裙半身裙', 79.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module5', $r('app.media.shop3'), '百褶裙半身裙', 139.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module6', $r('app.media.shop4'), '百褶裙半身裙', 145.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module7', $r('app.media.shop5'), '百褶裙半身裙', 289.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module8', $r('app.media.shop6'), '百褶裙半身裙', 389.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module7', $r('app.media.shop7'), '百褶裙半身裙', 289.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module8', $r('app.media.shop8'), '百褶裙半身裙', 389.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', '',),
  new ModuleItem('module1', $r('app.media.shop9'), '百褶裙半身裙', 109.99, 'Guozii秋冬新款美式复古撞色拼接设计t恤女2023早秋宽松休闲上衣', ''),
  new ModuleItem('module2', $r('app.media.shop10'), '百褶裙半身裙', 129.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', ''),
  new ModuleItem('module2', $r('app.media.shop10'), '百褶裙半身裙', 129.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙', ''),
]

export const shopList2: Array<ModuleItem> =  [
  new ModuleItem('module1',$r('app.media.shop1'), '百褶裙半身裙',109.99, 'Guozii秋冬新款美式复古撞色拼接设计t恤女2023早秋宽松休闲上衣' ),
  new ModuleItem('module2',$r('app.media.shop2'), '百褶裙半身裙',129.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module3',$r('app.media.shop3'), '百褶裙半身裙', 99.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module4',$r('app.media.shop4'), '百褶裙半身裙', 79.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module5',$r('app.media.shop5'), '百褶裙半身裙', 139.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module6',$r('app.media.shop6'), '百褶裙半身裙', 145.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module7',$r('app.media.shop7'), '百褶裙半身裙', 289.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module8',$r('app.media.shop8'), '百褶裙半身裙', 389.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module1',$r('app.media.shop9'), '百褶裙半身裙',109.99, 'Guozii秋冬新款美式复古撞色拼接设计t恤女2023早秋宽松休闲上衣'),
  new ModuleItem('module2',$r('app.media.shop10'), '百褶裙半身裙',129.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module3',$r('app.media.shop1'), '百褶裙半身裙', 99.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module4',$r('app.media.shop2'), '百褶裙半身裙', 79.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module5',$r('app.media.shop3'), '百褶裙半身裙', 139.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module6',$r('app.media.shop4'), '百褶裙半身裙', 145.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module7',$r('app.media.shop5'), '百褶裙半身裙', 289.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module8',$r('app.media.shop6'), '百褶裙半身裙', 389.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module7',$r('app.media.shop7'), '百褶裙半身裙', 289.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module8',$r('app.media.shop8'), '百褶裙半身裙', 389.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module1',$r('app.media.shop9'), '百褶裙半身裙',109.99, 'Guozii秋冬新款美式复古撞色拼接设计t恤女2023早秋宽松休闲上衣'),
  new ModuleItem('module2',$r('app.media.shop10'), '百褶裙半身裙',129.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module3',$r('app.media.shop1'), '百褶裙半身裙', 99.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module4',$r('app.media.shop2'), '百褶裙半身裙', 79.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module5',$r('app.media.shop3'), '百褶裙半身裙', 139.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module6',$r('app.media.shop4'), '百褶裙半身裙', 145.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module7',$r('app.media.shop5'), '百褶裙半身裙', 289.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module8',$r('app.media.shop6'), '百褶裙半身裙', 389.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module7',$r('app.media.shop7'), '百褶裙半身裙', 289.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module8',$r('app.media.shop8'), '百褶裙半身裙', 389.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module1',$r('app.media.shop9'), '百褶裙半身裙',109.99, 'Guozii秋冬新款美式复古撞色拼接设计t恤女2023早秋宽松休闲上衣'),
  new ModuleItem('module2',$r('app.media.shop10'), '百褶裙半身裙',129.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module3',$r('app.media.shop1'), '百褶裙半身裙', 99.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module4',$r('app.media.shop2'), '百褶裙半身裙', 79.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module5',$r('app.media.shop3'), '百褶裙半身裙', 139.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module6',$r('app.media.shop4'), '百褶裙半身裙', 145.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module7',$r('app.media.shop5'), '百褶裙半身裙', 289.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module8',$r('app.media.shop6'), '百褶裙半身裙', 389.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module7',$r('app.media.shop7'), '百褶裙半身裙', 289.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module8',$r('app.media.shop8'), '百褶裙半身裙', 389.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
  new ModuleItem('module1',$r('app.media.shop9'), '百褶裙半身裙',109.99, 'Guozii秋冬新款美式复古撞色拼接设计t恤女2023早秋宽松休闲上衣'),
  new ModuleItem('module2',$r('app.media.shop10'), '百褶裙半身裙',129.99, '大码咖色百褶裙半身裙女秋冬2023新款高腰显瘦百褶学院风a字短裙'),
]
function $r(arg0: string): any {
throw new Error('Function not implemented.')
}
