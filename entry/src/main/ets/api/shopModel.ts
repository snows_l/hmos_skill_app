import request from '../utils/request'

export const getList = (data = {})  => {
  return request({
    url: "/api/public/v1/goods/search",
    method: "get",
    params:data,
  });
}
