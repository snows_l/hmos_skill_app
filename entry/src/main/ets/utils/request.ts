import axios from '@ohos/axios'

const service  = axios.create(
  {
    baseURL: 'https://api-hmugo-web.itheima.net',
    timeout: 80000, // request timeout
  }
)

service.interceptors.request.use(
  (config) => {
   return config
  },
  (error) => {
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    // Message.error(error);
    return Promise.reject(error);
  }
);
export default service;