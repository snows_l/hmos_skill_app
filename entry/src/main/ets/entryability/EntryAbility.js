import UIAbility from '@ohos.app.ability.UIAbility';
import hilog from '@ohos.hilog';
const userInfo = {
    userName: 'snows_l',
    userId: '100107',
    entName: '企业名称',
    entId: '5306100107',
    roles: ['admin', 'caiwu'],
    work: {
        desc: '前端开发攻城狮',
        money: '1K',
    },
};
// 本地存储
AppStorage.SetOrCreate('userInfo', JSON.stringify(userInfo));
AppStorage.SetOrCreate('name', 'snows_l');
export default class EntryAbility extends UIAbility {
    /**
     * @description 应用生命周期
     */
    // 应用被打开的时候
    onCreate(want, launchParam) {
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onCreate');
    }
    // 应用被杀后台的时候
    onDestroy() {
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onDestroy');
    }
    onWindowStageCreate(windowStage) {
        // Main window is created, set main page for this ability
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageCreate');
        windowStage.loadContent('pages/Index', (err, data) => {
            var _a, _b;
            if (err.code) {
                hilog.error(0x0000, 'testTag', 'Failed to load the content. Cause: %{public}s', (_a = JSON.stringify(err)) !== null && _a !== void 0 ? _a : '');
                return;
            }
            hilog.info(0x0000, 'testTag', 'Succeeded in loading the content. Data: %{public}s', (_b = JSON.stringify(data)) !== null && _b !== void 0 ? _b : '');
        });
    }
    onWindowStageDestroy() {
        // Main window is destroyed, release UI related resources
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageDestroy');
    }
    // 应用被唤起的时候 （其他应用切换回来）
    onForeground() {
        // Ability has brought to foreground
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onForeground');
    }
    // 应用被挂在后盖的时候（切换到其他应用）
    onBackground() {
        // Ability has back to background
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onBackground');
    }
}
//# sourceMappingURL=EntryAbility.js.map